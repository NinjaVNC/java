FROM openjdk:11.0.11-jre-slim

RUN apt update
#RUN apt install -y xserver-common
RUN apt install -y xvfb
RUN apt install -y libxtst6
RUN apt install -y libxrender1

COPY build/libs/*.jar .
COPY docker-run.sh .
RUN chmod a+x docker-run.sh

ENTRYPOINT ["./docker-run.sh"]